from scraper import Scraper
import bs4
from bs4 import BeautifulSoup
import sys
from datetime import datetime
import unittest
import time
import re
import sys
import os
from datetime import date
from dateutil.rrule import rrule, DAILY
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# we initialise a bot that will have run the parsing functions

foo = Scraper("foo", start_date="3/12/2017", end_date="3/12/2017")
driver =webdriver.Firefox()
url = "http://192.92.176.8/cgi-bin/osearchd.mbr/input"
from_date=" "
to_date=" "
apn=" "
county="Monterey"
state="CA"
# below write the parsing functions for the data in the webpages for the webpage

def format_date(date):
    date=str(date)
    date_list=date.split("-")
    return date_list

def parse_page1():

    global from_date
    global to_date

    #from_date_list=format_date(from_date)
    #to_date_list=format_date(to_date)
    for dt in rrule(DAILY, dtstart=from_date, until=to_date):
        driver.get(url)
        month=driver.find_element_by_xpath("/html/body/div/form/table[2]/tbody/tr/td/input[2]")
        day=driver.find_element_by_xpath("/html/body/div/form/table[2]/tbody/tr/td/input[3]")
        year=driver.find_element_by_xpath("/html/body/div/form/table[2]/tbody/tr/td/input[4]")
        date_formatted=(dt.strftime("%Y-%m-%d"))
        curr_date_list=date_formatted.split("-")
        year.send_keys(curr_date_list[0])
        month.send_keys(curr_date_list[1])
        day.send_keys(curr_date_list[2])
        button=driver.find_element_by_xpath("/html/body/div/form/table[2]/tbody/tr/td/input[5]")
        button.click()
        time.sleep(2)
        paginate=True
        COUNTER=0 #PAGINATING FOR 2 PAGES
        while paginate:
            i=2
            row=[]
            link_list=[]
            element_content=[]
            ####COMMENT BELOW TO RUN FOR ALL ITERS AS CONTENT IS TOO LARGE!
            if (COUNTER==4):
                    break
            COUNTER=COUNTER+1
            while (1):
                    try:
                        row.append(driver.find_element_by_xpath("/html/body/form/p/table/tbody/tr"+"["+str(i)+"]"))
                    except:
                        break
                    else:
                        i=i+1
            row=row[1:]
            for curr_row in row:
                    element_content[:] = []
                    for j in range(1,6):
                        try:
                            element=curr_row.find_element_by_xpath(".//td["+str(j)+"]"+"/font")
                        except:
                            break
                        else:
                            if(j!=5 and j!=3):
                                element_content.append(element.text)
                            if (j==1):
                                link=driver.find_element_by_link_text(str(element.text))
                                link_list.append(link)
                            if (j==5):
                                list_grantor_grantee=element.text.split("\n")
                                list_grantor=[]
                                list_grantee=[]
                                for name in list_grantor_grantee:
                                    if (name[-3:]=="(R)"):
                                        list_grantor.append(name)
                                    else:
                                        list_grantee.append(name)
                                for name in list_grantor:
                                    element_content.append("Grantor")
                                    element_content.append(name)
                                    element_content.append(apn)
                                    element_content.append(county)
                                    element_content.append(state)
                                    elem = dict(zip(foo.headers, element_content))
                                    print (elem)
                                    yield elem
                                    for i in range(0,5):  ###POP state,county,apn,name,role
                                        element_content.pop()
                                for name in list_grantee:
                                    element_content.append("Grantee")
                                    element_content.append(name)
                                    element_content.append(apn)
                                    element_content.append(county)
                                    element_content.append(state)
                                    elem = dict(zip(foo.headers, element_content))
                                    print (elem)
                                    yield elem
                                    for i in range(0,5):
                                        element_content.pop()
                            #elem = driver.find_element_by_link_text("NEXT 10")
            try:
                elem = driver.find_element_by_link_text("NEXT 10")
            except:
                paginate=False
            else:
                elem.click()
                time.sleep(4)#wait to paginate
                            #elem = dict(zip(foo.headers, element_content))
                            #yield elem
                        #print(element.text)


# put the functions in a list where the sequence should be in the order that you
# want the bot to crawl through the pages

parse_functions = [parse_page1]

# define the headers for the csv file that will be generated

foo.headers =     ["doc_number",
                    "date",
                    "document",
                    "role",
                    "name",
                    "apn",
                    "county",
                    "state"
                    ]
# “doc_number”,"date",“document”, “role”, “name”, “apn”, “county”, and “state”
#print ('Number of arguments:', len(sys.argv), 'arguments.')
#print ('Argument List:', str(sys.argv))


if __name__ == "__main__":
    ERROR_MESSAGE="Please run as:sudo python3 county_name.py -f 12012016 -t 12012016. -f is the from date, -t is the to date"

    try:
    		from_date = datetime.strptime(str(sys.argv[2]), '%d%m%Y').date()
    except ValueError:
    	    sys.exit(ERROR_MESSAGE)
    try:
    		to_date = datetime.strptime(str(sys.argv[4]), '%d%m%Y').date()
    except ValueError:
    	    sys.exit(ERROR_MESSAGE)

    if (from_date>to_date):
        sys.exit("From date cannot be greater than To date")
    foo.run(parse_functions)
